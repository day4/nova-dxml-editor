let mix = require('laravel-mix')

mix.extend('override', function(webpackConfig, cb) {
  cb(webpackConfig)
});

mix
  .override(config => {
    console.log('-REMOVING SVG-')
    config.module.rules.find(rule =>
      rule.test.test('.svg')
    ).exclude = /\.svg$/;

    console.log('-ADDING SVG-')
    config.module.rules.push({
      test: /\.svg$/,
      use: [{ loader: 'html-loader' }]
    })
  })
  .babelConfig({
    plugins: ['@babel/plugin-syntax-dynamic-import'],
  })
  .webpackConfig({
    output: {
      chunkFilename: "js/chunks/[id].chunk.js?id=[chunkhash]",
      jsonpFunction: "day4dxmleditor" //https://github.com/webpack/webpack/issues/9766
    }
  })
  .setPublicPath('dist')
  .js('resources/js/field.js', 'js')
  .sass('resources/sass/field.scss', 'css')