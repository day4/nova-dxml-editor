<?php

namespace Day4\DxmlEditor;

use Laravel\Nova\Fields\Field;

class DxmlEditor extends Field
{
    /**
     * The field's component.
     *
     * @var string
     */
    public $component = 'dxml-editor';

    /**
     * Hide custom components from being available in the editor
     *
     * @param array $components Names of the components to remove
     * @return $this
     */
    public function hide(array $components)
    {
        return $this->withMeta(['hidden' => $components]);
    }
}
