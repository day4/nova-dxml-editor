Nova.booting((Vue, router, store) => {
  Vue.prototype.$fetch = require('./_Fetch.js').default
  require('../sass/field.scss')

  require('./dxmlComps/index.js').default(Vue)

  Vue.use(require('dxml-editor/src').default)
  Vue.component('index-dxml-editor', require('./components/IndexField').default)
  Vue.component('detail-dxml-editor', require('./components/DetailField').default)
  Vue.component('form-dxml-editor', require('./components/FormField').default)
})