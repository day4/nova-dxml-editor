
const tmpCache = {}
function hashCode(str) {
  let hash = 0;
  for (let i = 0; i < str.length; i++) {
    let character = str.charCodeAt(i);
    hash = ((hash<<5)-hash)+character;
    hash = hash & hash; // Convert to 32bit integer
  }
  return hash;
}

export default function(query, variables = {}, cache) {
  const body = JSON.stringify({
    variables,
    query,
  })
  let hash = null;
  if (cache) {
    hash = hashCode(body);
    if (tmpCache[hash] && Date.now() < tmpCache[hash].ts) {
      return Promise.resolve(JSON.parse(tmpCache[hash].result))
    }
  }

  return fetch('/graphql', {
    method: 'POST',
    body,
    mode: 'cors',
    credentials: 'include',
    headers: {
      'Content-Type': 'application/json'
    }
  }).then(res => res.json()).then(obj => {
    if (obj.errors) {
      console.warn(obj.errors.map(e => e.message).join(' & '))
      throw obj.errors
    } else {
      if (cache && hash) {
        tmpCache[hash] = {
          ts: Date.now() + 300000,
          result: JSON.stringify(obj.data)
        }
      }
      return obj.data
    }
  })
}